#!/usr/bin/env python3
# Copyright 2019-2022, Collabora, Ltd.
# SPDX-License-Identifier: BSL-1.0
#
# Original Author: Rylie Pavlik <rylie.pavlik@collabora.com>
"""Make a standalone .rules file."""
from xrhardware.db import get_devices
from xrhardware.generate import generate_rules_file_contents

if __name__ == "__main__":
    all_rules = "\n\n".join(d.make_commented_rule() for d in get_devices())
    contents = "\n".join(
        (
            "# BEGIN DEVICE LIST #",
            "#####################",
            all_rules,
            "###################",
            "# END DEVICE LIST #",
        )
    )
    print(generate_rules_file_contents(__file__, "70-xrhardware.rules", contents))
